
package practica;

import javax.swing.JOptionPane;


public class Practica {
    
    public static void main(String[] args) {
        dolarCambio();
    }
    
    public static void dolarCambio(){
        String mensaje = "";
        for(int i = 1; i<=10; i++){
            float tipocambio = i * 605;    
            mensaje = mensaje + "El tipo de cambio para " + i + " dolar(es) es: " + tipocambio + "\n";
        }
        JOptionPane.showMessageDialog(null, mensaje);
    }
    
}
